# ASP project: "Minesweeper goes Kaboom"

## Status
- [x] model validator

- [x] model recommender

- [x] implement game flow (python)

- [x] visualize game

## Description
This repository belongs to a student project which concerns ASP (answer set programming) for a variation of the game Minesweeper called [Kaboom](https://pwmarcz.pl/kaboom/).
The task is to implement the game by utilising ASP as it stands in contrast to an [existing approach using SAT](https://cse.unl.edu/~minesweeper/version1/minesweeper/) (see Bayer, Kenneth M., Josh Snyder, and Berthe Y. Choueiry. "An Interactive Constraint-Based Approach to Minesweeper." AAAI. 2006).

## Usage
The current status of this project can best be viewed by calling

`cd pyth/src`

from the parent directory, and then

`python3 main.py`

## Requirements
- Python 3.10 or Python 3.11
- Clingo

## Structure
### ASP
`kaboom.lp` contains some general rules of the problem.
In addition to the `validator.lp` which deduces all guaranteed mines and safe tiles, there are some smaller ASP files for inferring edges, mine candidates and, as for `recommender.lp`, outputs helper predicates for recommendation of a good move.

Seperate from the game logic, every file named `inst*.lp`, `eval*.lp`, or `stat*.lp` is an instance of a problem, i.e. a game board.
See `asp/instances/inst-template.lp` for an explanation of an instances structure.

### Python
The Python part mainly consists of two classes Game (in game.py, providing implementation for Kaboom) and Gui (in gui.py, for visualization in curses).
The module main.py uses both classes to provide the full game. 
Within the `pyth/src/` folder, there are `asp.py` (taken from [Benjamin Johnston](https://www.benjaminjohnston.com.au/pythonasp)) which implements a wrapper for calling clingo in python (the `clingo_path` depends on your system, so you may need to change it), and the `callasp.py`, which reads the specified `.lp` files and calls the wrapper, which returns all answer sets.
