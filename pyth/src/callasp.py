import asp
import sys


def main(list_of_files):
    # read files
    pre = '../../asp/'
    files = list_of_files
    code = ""

    for fi in files:
        with open(pre+fi) as f:
            content = f.read().splitlines()

        for line in content:
            code = code + line + '\n'

    # call clingo
    solutions = asp.solve(code)

    return solutions


if __name__ == "__main__":
    pass
