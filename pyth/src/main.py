import curses
from curses import wrapper
import math as m

import game as g
import gui as gui

# method to handle m - mark as mine
def mark(ui, game):
    y,x = ui.wboard.getyx()
    i,j = ui.pos_to_ind(y,x)
    put_marking = game.mark( (i,j) )
    if put_marking == None:
        pass
    elif put_marking:
        ui.wboard.addch('m', curses.color_pair(198))
    else:
        ui.wboard.addch(game.hidden_char)
    ui.wboard.refresh()
    update_mark_count(ui, game)


# method to handle n - new game
def set_up_game(ui, bi, bj, mi):
    # check parameters for feasability
    if (bi*bj) < 2:
        ui.display3("Unappropriate size")
        return None
    elif (mi >= (bi * bj)) or (mi < 1):
        message = "Mines may range from 1 to " + str(bi*bj-1)
        ui.display3(message)
        return None
    else:
        game = g.Game(bi, bj, mi)
        ui.display(game)

        ui.wstatus1.clear()
        ui.wstatus1.refresh()
        ui.wstatus2.clear()
        ui.wstatus2.refresh()
        ui.wstatus3.clear()
        ui.wstatus3.refresh()

        # put cursor in the middel
        posy, posx = ui.ind_to_pos( int((bi-1)/2), int((bj-1)/2) )
        ui.wboard.move(posy, posx)
        ui.wboard.refresh()
        curses.curs_set(True)
        
        return game, posy, posx


# method to handle x - making a move
def reveal_tile(ui, game):
    y,x = ui.wboard.getyx()
    i,j = ui.pos_to_ind(y,x)
    game.update_wrapper( (i,j) )
    ui.display(game)
    ui.wstatus2.clear()
    ui.wstatus2.refresh()


### HELPER:

def update_mark_count(ui, game):
    ui.wstatus1.clear()
    ui.wstatus1.addstr("mines: " + str(game.get_num_marked()) + "/" + str(game.num_mines))
    ui.wstatus1.refresh()
    
    ui.wboard.border(' ', ' ', 0, ' ', curses.ACS_HLINE, curses.ACS_HLINE, ' ', ' ')
    ui.wboard.refresh()



def main(stdscr):
    guiloop = True
    refresh_asp = True
    loss = False
    win = False

    ui = gui.Gui(stdscr)
    game, y, x = set_up_game(ui, 10,10,10)
    ui.stdscr.refresh()


    while guiloop:
        if refresh_asp:
            refresh_asp = False
            game.py_to_asp(game.board)
            game.validate()
            ui.display(game)

            game.check_win()
            if game.state is g.GameState.WIN:
                ui.display3("you win")
            if game.state is g.GameState.LOSE:
                ui.display3("kaboom")

        c = stdscr.getch()
        if c == ord('a') or c == ord('h'):
            x -= 2
            if x < 1:
                x = 1
        elif c == ord('d') or c == ord('l'):
            x += 2
            if x > (2 * game.bj -1):
                x = 2 * game.bj -1
        elif c == ord('D'):
            game.switch_debug()
            if game.debug:
                ui.display3("debug on")
            else:
                ui.wstatus3.clear()
                ui.wstatus3.refresh()
            ui.display(game)
        elif c == ord('H'):
            ui.display2(game.show_hint())
        elif c == ord('m'):
            mark(ui, game)
        elif c == ord('n'):
            try:
                (bi, bj, mi) = ui.take_user_input()
                bi = int(bi)
                bj = int(bj)
                mi = int(mi)
                game, y, x = set_up_game(ui, int(bi), int(bj), int(mi))
                refresh_asp = True
            except:
                pass
        elif c == ord('q'):
            guiloop = False
        elif c == ord('r'):
            if game.state is g.GameState.PENDING:
                rec = game.recommend()
                (y,x) = ui.highlight(rec, True)
            else:
                pass
        elif c == ord('s') or c == ord('j'):
            y += 1
            if y > game.bi:
                y = game.bi
        elif c == ord('u'):
            bo = game.undo()
            if bo:
                refresh_asp = True
                update_mark_count(ui, game)
                ui.display2("")
                ui.display3("")
            else:
                ui.display2("history empty")
        elif c == ord('w') or c == ord('k'):
            y -= 1
            if y < 1:
                y = 1
        elif c == ord('x'):
            reveal_tile(ui, game)
            refresh_asp = True
            update_mark_count(ui, game)
        
        ui.wboard.move(y,x)
        ui.wboard.refresh()



if __name__ == "__main__":
    wrapper(main)
