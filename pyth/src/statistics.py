# this is a script for generating data in order to evaluate
# which recommender strategy should be used

import callasp
import copy
import game as g


# print head of table
se = ", " # separator
head = "inst{s}eb{s}sb{s}mb{s}clearity_b{s}choice_i{s}choice_j{s}num_nei_e{s}\
num_nei_f{s}ea{s}sa{s}ma{s}clearity_a{s}gain\n".format(s=se)

with open("recdata100", 'w', encoding="utf-8") as f:
    f.write(head)

# choose a few instances to test on
instances = []
for i in range(19):
    tmp = "stat" + str(i+1) + ".lp"
    instances.append(tmp)

game = g.Game(2,2,2)

# iterate through instances
for num,inst in enumerate(instances):
    # initialize instance
    game.asp_to_py(inst)
    game.py_to_asp(game.board)

    # collect instance parameters
    answers = callasp.main(["kaboom.lp", "instances/inst-tmp.lp", "edges.lp", "validator.lp", "candidates.lp"])
    game.ans_solutions = answers
    ans = [set(tmp) for tmp in answers]
    insc = set.intersection(*ans)
    (s,m,e,c) = game.sort_answers([list(insc)])
    game.ans_mines = m
    game.ans_safes = s

    # those are the parameters:
    num_e_before = len(e)
    num_s_before = len(s)
    num_m_before = len(m)
    # a quantization of information: known tiles vs all edge tiles
    clearity_before = float(num_s_before+num_m_before)/float(num_e_before)

    # count measures before choice
    rec_answers = callasp.main(["kaboom.lp", "instances/inst-tmp.lp", "recommender.lp", "edges.lp"])
    edges, flags = g.sort_rec_answers(rec_answers)


    # test every possible move
    target = g.valid_choices()
    for (i,j) in target:
        num_nei_e, num_nei_f = g.count_resp_nei(edges, flags, (i,j))


        for repetition in range(100):

            game.update_wrapper((i,j))

            # count the stuff anew
            answers2 = callasp.main(["kaboom.lp", "instances/inst-tmp.lp", "edges.lp", "validator.lp", "candidates.lp"])
            answers2 = [set(tmp) for tmp in answers2]
            insc = set.intersection(*answers2)
            (s2,m2,e2,c2) = game.sort_answers([list(insc)])
            game.ans_mines = m2
            game.ans_safes = s2

            # those are the parameters:
            num_e_after = len(e2)
            num_s_after = len(s2)
            num_m_after = len(m2)
            # a quantization of information: known tiles vs all edge tiles
            clearity_after = float(num_s_after+num_m_after)/float(num_e_after)
            # undo the step
            game.undo()

            # write data to file
            dataline = se.join([str(num+1),str(num_e_before),\
                    str(num_s_before),str(num_m_before),\
                    str(clearity_before), str(i),str(j),\
                    str(num_nei_e),str(num_nei_f),\
                    str(num_e_after),str(num_s_after),\
                    str(num_m_after),str(clearity_after),\
                    str(clearity_after-clearity_before)])
            dataline += "\n"

            with open("recdata100", 'a', encoding="utf-8") as f:
                f.write(dataline)
