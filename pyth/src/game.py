import copy
from enum import Enum
import random
import sys

import callasp

# methods:
#    py_to_asp(board)
#    asp_to_py(file)
#    validate()
#    validate_light()
#    recommend()
#    valid_choices()
#    process_input()
#    switch_debug()
#    mark(move)
#    get_num_marked()
#    undo()
#    update_wrapper(move)
#    update(move)
#    generate_mines(move)
#    infer_flags(move, samples, fifo, bool_update)
#    show_hint()
#    get_debug()
#    ts(ll)
#    board_to_string(board)
#    sort_answers(answers)
#    sort_rec_answers(answers)
#    count_resp_nei(edges, flags, move)
#    get_revealed()
#    check_win()

""" A logger has a log file and a log_bool that says whether it is active.
An inactive logger won't write any lines into the log file.
This class is meant for debugging and has to be activated if needed,
i.e. set the boolean in the __init__ method of Game.
"""
class Logger:
    def __init__(self, file, log_bool):
        self.file = file
        self.active = log_bool

    def log(self, string):
        if self.active:
            with open(self.file, 'a', encoding="utf-8") as f:
                f.write(string)


""" The GameState class provides a data structure to express the game status
of an instance of the Game class.
A game can therefore be running (pending), or be over in the form of
won, lost or exited by the player.
"""
class GameState(Enum):
    EXIT = 0
    PENDING = 1
    WIN = 2
    LOSE = 3


""" The Kaboom Game class implements all the methods that are needed in order
to play Kaboom in the terminal. There is also a version with a more fancy
terminal gui (that can be played by executing python main.py) that also uses
those methods.
Apart from that, game.py can be executed on its own.
"""
class Game:

    def __init__(self, bi, bj, mines):
        self.mine_char = "M"
        self.hidden_char = '*'
        self.safe_char = "_"

        self.board = [[self.hidden_char for j in range(bj)] for i in range(bi)]
        self.marks = [] # holds positions of marked tiles
        self.ans_solutions = [] # holds all answer sets
        self.ans_safes = [] # holds safe fields from the answer sets intersect.
        self.ans_mines = [] # holds mines from the answer sets intersection 
        self.ans_edges = [] # edges from the anser set
        self.ans_cands = [] # mine candidates
        self.nei_e = [] # holds the neighboring edges
        self.nei_f = [] # holds the neighboring flags
        self.history = [] # for undo function
        self.commands = ["move", "undo", "mark", "hint", "rec", "debug", "exit"]

        self.bi = bi
        self.bj = bj
        self.num_mines = mines # the amount (number) of mines

        self.state = GameState.PENDING
        self.debug = False

        self.logger = Logger("log", False)
        # mark the start of this game's log
        self.logger.log("\n================================================")


    def __str__(self):
        return self.ts(self.board)


    """ Converts python game board instance into a .lp file.

    Parameters
    board: List<List>
        The game board
    """
    def py_to_asp(self, board):
        with open("../../asp/instances/inst-tmp.lp", 'w', encoding="utf-8") as f:
            # declare const
            f.write("#const bi=" + str(self.bi) + ".\n")
            f.write("#const bj=" + str(self.bj) + ".\n")
            f.write("#const mines=" + str(self.num_mines) + ".\n")

            # now generate the flags
            for i in range(self.bi):
                f.write("\n")
                for j in range(self.bj):
                    if board[i][j] in range(9):
                        f.write("flag(" + \
                                str(board[i][j]) + \
                                "," + \
                                str(i+1) + \
                                "," + \
                                str(j+1) + \
                                "). " )
            f.write("\n")


    """ Reads an .lp file into the game.board and refreshes the game state.

    Parameters
    file: String
        The name of the file to get the new game board from
    """
    def asp_to_py(self, file):
        with open("../../asp/instances/"+file, 'r', encoding="utf-8") as f:
            for num, line in enumerate(f):

                # first read instance parameters
                if num == 0:
                    _,b = line.split('=')
                    self.bi = int(b.strip().replace('.',''))
                elif num == 1:
                    _,b = line.split('=')
                    self.bj = int(b.strip().replace('.',''))
                elif num == 2:
                    _,b = line.split('=')
                    self.num_mines = int(b.strip().replace('.',''))

                    # now we reset the entire game instance
                    self.board = [[self.hidden_char for j in range(self.bj)] for i in range(self.bi)]
                    self.marks = []
                    self.ans_solutions = []
                    self.ans_safes = []
                    self.ans_mines = []
                    self.history = []
                    self.state = GameState.PENDING
                    self.debug = False

                # now read the flags
                if num > 2:
                    if line != "\n":
                        flags = line.strip().split('.')
                        for flag in flags:
                            try:
                                flag = flag.replace("flag(", '').replace(')','').replace(' ','')
                                f, i, j = flag.strip().split(',')

                                # update the flag on the selected field
                                self.board[int(i)-1][int(j)-1] = int(f)
                            except:
                                pass


    """ Calls the validator.lp (which provides the guarenteed safes and mines)
    and writes them to the class attributes. At the same time calls all the
    other asp code, too.

    Parameters
    board: List<List>
        In most cases the current game board
    """
    def validate(self):
        del self.ans_solutions[:]

        del self.ans_safes[:]
        del self.ans_mines[:]
        del self.ans_edges[:]
        del self.ans_cands[:]

        del self.nei_e[:]
        del self.nei_f[:]

        solutions = callasp.main(["kaboom.lp", "instances/inst-tmp.lp",\
                "validator.lp", "recommender.lp", "edges.lp", "candidates.lp"])
        self.ans_solutions = solutions

        # compute the intersection of all answer sets
        solutions = [set(tmp) for tmp in solutions]
        insc = set.intersection(*solutions)

        (s,m,e,c) = self.sort_answers([list(insc)])
        self.ans_safes = s
        self.ans_mines = m
        self.ans_edges = e
        self.ans_cands = c

        (ne, nf) = self.sort_rec_answers([list(insc)])
        self.nei_e = ne
        self.nei_f = nf
        

    """ Same as validate, but only calling the validator.lp and kaboom.lp

    Parameters
    board: List<List>
        The current game board
    """
    def validate_light(self, board):
        del self.ans_safes[:]
        del self.ans_mines[:]

        solutions = callasp.main(["kaboom.lp", "instances/inst-tmp.lp",\
                "validator.lp"])

        # compute the intersection of all answer sets
        solutions = [set(tmp) for tmp in solutions]
        insc = set.intersection(*solutions)

        (s,m,_,_) = self.sort_answers([list(insc)])
        self.ans_safes = s
        self.ans_mines = m
        

    """ Suggests a move based on the data from the recommender that is called
    with the validator. The data in question are stored in ans.nei_e and
    ans.nei_f

    Returns
    r: Tuple<int,int>
        A move that is recommended next
    """
    def recommend(self):
        valids = self.valid_choices()

        max_e = 0
        min_f = 8
        # contains good moves o.t.f. [ ((i,j),e,f), ... ]
        good_moves = []
        # in case the heuristic won't find a good move
        r = valids[0]

        # first iteration looks for good e
        for move in valids:
            e, f = self.count_resp_nei(self.nei_e, self.nei_f, move)
            if e > max_e:
                max_e = e
                tup = (move,e,f)
                # previous good_moves with less e a worse
                good_moves = []
                good_moves.append(tup)
            if e == max_e:
                tup = (move,e,f)
                good_moves.append(tup)

        # all moves in good_moves should now have the same e
        # next is optimizing for f
        for move in good_moves:
            if (move[1] < min_f) and (move[1] > 0):
                min_f = move[1]
                r = move[0]

        return r 


    """ Returns the set of valid moves.
    The method validator should have been called before!

    Returns
    vc: List<Tuple<int,int>>
        A list of moves that are valid choices according
        to the current game board
    """
    def valid_choices(self):
        vc = []
        # A move must be chosen either from
        # 1. the set of safe tiles if there are any
        if len(self.ans_safes) > 0:
            vc = copy.deepcopy(self.ans_safes)

        # 2. edges that are no mines if there are any
        elif (len(self.ans_edges) - len(self.ans_mines)) > 0:
            vc = [x for x in self.ans_edges if x not in self.ans_mines]

        # 3. any tile that is not a mine (in this case matches
        # with the candidates without flags
        else:
            flags = self.get_revealed()
            vc = [x for x in self.ans_cands if x not in flags]

        return vc


    """ Gives input promts and calls the respective function.
    """
    def process_input(self):
        print("possible commands:     ", end='')
        for e in self.commands:
            print('[' + e + ']   ', end='')

        inp = input("\nenter command: ")
        match inp:
            case "move":
                mi = input("Uncover field in row: ")
                mj = input("and column: ")
                try:
                    # transfer directly to indices (start counting at 0)
                    move = (int(mi)-1, int(mj)-1)
                except:
                    print("please enter only integers")
                    return
                self.update_wrapper(move)
            case "rec":
                try:
                    r = self.recommend()
                    message = "Reveal (" + str(r[0]+1) + ", " + str(r[1]+1) + ")?"
                    inp = input(message + " [Y/n] > ")
                    match inp:
                        case "n":
                            pass
                        case _:
                            self.update(r)
                except:
                    raise ValueError("No valid moves")
            case "mark":
                mi = input("Mark field as mine in row: ")
                mj = input("and column: ")
                try:
                    # transfer directly to indices (start counting at 0)
                    move = (int(mi)-1, int(mj)-1)
                except:
                    print("please enter only integers")
                    return
                self.mark(move)
            case "hint":
                print(self.show_hint())
            case "debug":
                self.switch_debug()
            case "exit":
                self.state = GameState.EXIT
            case "undo":
                bo = self.undo()
                if not bo:
                    print("history empty")


    """ Switches the game state between "in debug mode" and "in normal mode".
    """
    def switch_debug(self):
        if self.debug:
            self.debug = False
        else: self.debug = True


    """ Used to mark or unmark a field as mine as an aid for the player.

    Parameters
    move: Tuple<int,int>
        The respective tile to be marked

    Returns
    _: Bool
        Indicates whether the tile was marked or demarked
    """
    def mark(self, move):
        if self.board[move[0]][move[1]] == self.hidden_char:
            if move in self.marks:
                self.marks.remove(move)
                return False
            elif move not in self.marks:
                self.marks.append(move)
                return True
        return None


    """ Returns the amount of marked tiles as a help for the player.

    Returns
    count: int
        The amount of marked tiles or debugger's amount of mines respectively
    """
    def get_num_marked(self):
        if self.debug:
            # based on inferred mines
            count = len(self.ans_mines)
        else:
            # based on player's marks
            count = len(self.marks)
        
        return count


    """ Takes the last entry from the game history and sets it as the current game state.

    Returns
    _: False
        Only if it was not possible to pop the history, probably when the history is empty. This return provides a more flexible way for the "history empty" message to present (since the gui would want to print it differently)
    """
    def undo(self):
        try:
            self.board = self.history.pop()
            self.py_to_asp(self.board)
            self.validate()
            self.state = GameState.PENDING
            return True
        except:
            return False


    """ This method checks whether the move stated is a validated move.
    This is done by valid_choices.
    Based on those checks, this wrapper then either updates the move or lets
    the player lose if they guessed illegally.
    Part of the "update a move" pipeline.

    Parameters
    move: Tuple<int,int>
        A move represents a tile that is to be revealed
    """
    def update_wrapper(self, move):

        if move in self.valid_choices():
            self.update(move)
        elif move in self.marks:
            # we prevent guessing on a marked field
            pass
        elif self.board[move[0]][move[1]] is not self.hidden_char:
            # this case is stating an already discovered field -> ignore
            pass
        else:
            # we might want to undo the losing move
            self.history.append(copy.deepcopy(self.board))
            self.state = GameState.LOSE

    
    """ This method coordinates all task of updating a move, which mainly
    consists of generating possible mine positions and inferring flags based
    on those mines.
    Part of the "update a move" pipeline.

    Parameters
    move: Tuple<int,int>
        When the player wants to reveal a tile (i,j) aka when they make a move,
        a flag should be generated for that tile
    """
    def update(self, move):

        # recursion case might lead to
        if self.state == GameState.WIN:
            return

        # write old gameboard to history
        self.history.append(copy.deepcopy(self.board))

        # > first part: generate mines
        samples = self.generate_mines(move)

        self.logger.log("\n")
        self.logger.log(self.ts(self.board))
        self.logger.log("\nvalidator:\n")
        self.logger.log(self.ts(self.ans_solutions))
        self.logger.log("\nsamples:")
        self.logger.log(str(samples))

        # > second part: infer flags
        recursion_fifo = []
        self.infer_flags(move, samples, recursion_fifo, False)
        # refresh all self. parameters
        self.py_to_asp(self.board)
        self.validate()


    """ Chooses a random mine dicstributin that is generated by asp and,
    if necessary, generates additional mines.
    Part of the "update a move" pipeline.

    Parameters
    move: Tuple<int,int>
        Such as (i,j)

    Returns
    samples: List< Tuple<int,int> >
        A list of possible mine positions
    """
    def generate_mines(self, move):

        # choose one random answer set, it may be the empty set
        samp = random.sample(self.ans_solutions, 1)
        (_,m,_,_) = self.sort_answers(samp)

        # and generate the remaining mines from a pool of mine candidates
        rest_mines = random.sample(self.ans_cands, (self.num_mines - len(m)) )

        if move in (m + rest_mines):
            # try again
            return self.generate_mines(move)
        else:
            return m + rest_mines


    """ Infers the flag of the current tile and, if applicable, stages and
    calls further flag inferations for the neighboring tiles.
    Part of the "update a move" pipeline.

    Parameters
    move: Tuple<int,int>
        Describes a tile (i,j); the current tile
    samples: List< Tuple<int,int> >
        A list of tiles (i,j) with mines on them
    recursion_fifo: List< Tuple<int,int> >
        A list of tiles (i,j) with the next tiles to reveal
    update: Bool
        Says whether or not to update the answer set in the next recursion
    """
    def infer_flags(self, move, samples, recursion_fifo, update):

        self.logger.log("\n--------------start--------------")
        self.logger.log("\ninputs: move - " + str(move))
        self.logger.log("\nsamples - " + str(samples))
        self.logger.log("\nfifo - " + str(recursion_fifo))

        # count mines in neighborhood of input
        # and store potential recursion moves (breadth-first approach)
        flag = 0
        tmp_recursion = []
        for i in range(move[0] - 1, move[0] + 2):
            for j in range(move[1] - 1, move[1] + 2):
                try:
                    if ((i,j) in self.ans_mines) or ((i,j) in samples):
                        flag = flag + 1
                    # could we possibly stage mines for recursion?
                    # if so we rather put them into a temporary recursion fifo
                    if self.board[i][j] == self.hidden_char:
                        if ( (i,j) not in recursion_fifo) and ( (i,j) != move):
                            if (i >= 0) and (j >= 0) and (i < self.bi) and (j < self.bj):
                                tmp_recursion.append( (i,j) )
                except:
                    pass

        # update the flag on the selected field
        self.board[move[0]][move[1]] = flag
        # a revealed tile can't have a mark
        try:
            self.marks.remove( (move[0],move[1]) )
        except:
            pass

        self.logger.log("\ngame board:\n")
        self.logger.log(self.ts(self.board))

        update_ans_mines = True
        # automatically uncover fields with flag 0; recursion case
        if flag == 0:
            update_ans_mines = False

            # now it is safe to stage the neighbors of this move for recursion
            recursion_fifo.extend(tmp_recursion)

            self.logger.log("\ntry entering recursion: ")

        try:
            self.logger.log("\nrecursion successful ")
            self.logger.log("\n---------------end---------------")
            self.infer_flags( recursion_fifo.pop(0), samples, recursion_fifo, update_ans_mines )

        except:
            self.logger.log("\nrecursion FAILED")
            pass


    """ Prints whether it is allowed to guess or not.

    Returns
    _: str
        A message indicating whether or not to guess.
    """
    def show_hint(self):
        if self.ans_safes == []:
            return "you must guess"
        else:
            return "there are safe fields"


    """ Generates a game board version that shows which tiles are either
    mines or safe, doesn't show unknown.

    Returns
    debug: List<List>
        A board with characters self.mine_char and
        self.safe_char added to the self.board
    """
    def get_debug(self):
        debug = copy.deepcopy(self.board)

        for (m1,m2) in self.ans_safes:
            debug[m1][m2] = self.safe_char

        for (m1,m2) in self.ans_mines:
            debug[m1][m2] = self.mine_char

        return debug


    """ A to-string method. Takes a list of lists (ll) and prints it in a
    readable manner. A less fancy variant of board_to_string(board) meaning
    only printing the the necessary information

    Parameters
    ll: List<List>
        Could be any board like self.board
    """
    def ts(self, ll):
        res = ""

        for i in range(len(ll)):
            for j in range(len(ll[i])):
                if ll[i][j] == 0:
                    res = res + ' '
                else:
                    res = res + str(ll[i][j])
                res = res + ' '
            res = res + '\n'
        return res


    """ Generates any board's printable string.
    A more fancy variant of ts(ll)

    Parameters
    board: List<List>
        Those are self.board or self.representation

    Returns
    res: String
        A fancy print that aligns the fields vertically and horizontally just
        as a minesweeper board
    """
    def board_to_string(self, board):
        tmp = copy.deepcopy(board)
        for (i,j) in self.marks:
            tmp[i][j] = self.mine_char

        res = "\t"
        for j in range(self.bj):
            res = res + "{:<4}".format(str(j+1))
        res = res + '\n'

        for i in range(self.bi):
            res = res + str(i+1) + '\t'
            for j in range(self.bj):
                if tmp[i][j] == 0:
                    res = res + '[ ] '
                else:
                    res = res + '[' + str(tmp[i][j]) + '] '
            res = res + '\n'
        return res


    """ This helper function converts a list of strings into a bunch of lists
    that are sorted. Strings are predicates of arity 2.
    
    Parameters 
    ansset: List<String>
        E.g.: ['safe(1,2)', 'mine(3,4)']
        It is important that strings follow the pattern
        'name_of_any_length(int,int)'
    
    Returns
    (s,m,e,c): Tuple<List< Tuple<Int> >>
        Lists are containing tuples of indices o.t.f. (i,j)
    """
    def sort_answers(self, ansset):
        s = []
        m = []
        e = []
        c = []
        for l in ansset:
            for pred in l:
                pred = pred.replace('(', ',').replace(')', '').split(',')
                # pred now has entries of the form ["save", "i", "j"]
                mi = int(pred[1]) - 1
                mj = int(pred[2]) - 1

                match pred[0]:
                    case "safe":
                        s.append( (mi,mj) )
                    case "mine":
                        m.append( (mi,mj) )
                    case "edge":
                        e.append( (mi,mj) )
                    case "candidate":
                        c.append( (mi,mj) )

        return (s,m,e,c)

    
    """ This helper function converts a list of strings into a bunch of lists
    that are sorted. Strings are predicates of arity 4.
    
    Parameters 
    ansset: List<String>
        E.g.: ['nei_edge(1,2, 1,3)', 'nei_flag(3,4, 2,4)']
        It is important that strings follow the pattern
        'name_of_any_length(int,int,int,int)'
    
    Returns
    (e,f): Tuple<List< Tuple<Int> >>
        Lists are containing tuples (i,j,k,l)
    """
    def sort_rec_answers(self, ansset):
        edges = []
        flags = []
        for l in ansset:
            for pred in l:
                pred = pred.replace('(', ',').replace(')', '').split(',')
                # pred now has entries of the form
                # ["nei_flag", "i", "j", "k", "l"]
                mi = int(pred[1]) - 1
                mj = int(pred[2]) - 1
                try:
                    mk = int(pred[3]) - 1
                    ml = int(pred[4]) - 1
                except:
                    pass

                match pred[0]:
                    case "nei_edge":
                        edges.append( (mi,mj,mk,ml) )
                    case "nei_flag":
                        flags.append( (mi,mj,mk,ml) )
                    case _:
                        pass
        return (edges, flags)


    """ For a given move, counts the amount of neighboring
        flags and edges from lists of nei_flag and nei_edge
        predicates.

    Parameters
    edges: List<Tuple <int,int,int,int> >>
        Values (i,j,k,l) from the nei_edge relation
        stating that (k,l) is a neighboring edge of
        a tile (i,j)
    flags: List<Tuple <int,int,int,int> >>
        Values (i,j,k,l) from the nei_flag relation
        stating that (k,l) is a neighboring flag of
        a tile (i,j)
    move: Tuple<int,int>
        A move (i,j)

    Returns
    (e,f) : Tuple<int,int>
        With e the amount of neighboring edges and f
        the amount of neighboring flags
    """
    def count_resp_nei(self, edges, flags, move):
        i = move[0]
        j = move[1]
        num_edges = 0
        num_flags = 0
        for entry in edges:
            if entry[0] == i and entry[1] == j:
                num_edges += 1
        for entry in flags:
            if entry[0] == i and entry[1] == j:
                num_flags += 1
        
        return (num_edges, num_flags)


    """ Returns all flags from the current game.

    Returns
    flags : List<Tuple<int,int>>
        A list of all non-hidden tiles of the game board
    """
    def get_revealed(self):
        flags = []
        for i in range(self.bi):
            for j in range(self.bj):
                if self.board[i][j] in range(9):
                    flags.append( (i,j) )
        return flags


    """ Checks whether the game is won and sets the game state accordingly.
    """
    def check_win(self):
        count_hidden = 0
        for b in self.board:
            for field in b:
                if field == self.hidden_char:
                    count_hidden = count_hidden + 1

        if count_hidden == self.num_mines:
            self.state = GameState.WIN


if __name__ == "__main__":

    game = Game(220,220,5000)
    print("board explanation:\n \'" + game.hidden_char + "\' - hidden\n \'" + game.mine_char + "\' - " + \
            "mine\n [1-8] - flag\n [ ] - empty aka flag 0\n")

    if False: # examples instance
        game.asp_to_py("inst1.lp")

    while game.state == GameState.PENDING:

        game.py_to_asp(game.board)
        game.validate()

        if game.debug:
            print(game.board_to_string(game.get_debug()))
            print("debug: on")
        else:
            print(game.board_to_string(game.board))

        print(str(game.get_num_marked()) + "/" + str(game.num_mines) + " mines")

        game.process_input()
        game.check_win()


    if game.state == GameState.LOSE:
        print("kaboom")
    if game.state == GameState.WIN:
        print("you win")
