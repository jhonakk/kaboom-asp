import copy
import curses

class Gui:
    def __init__(self, stdscr):
        curses.start_color()
        curses.use_default_colors()
        curses.init_pair(249, 248, -1) # input
        curses.init_pair(198, 197, -1) # mines
        curses.init_pair(47, 18, 46) # recommended

        # abbreviations for screen
        self.stdscr = stdscr
        self.sx = curses.COLS
        self.sy = curses.LINES

        # draw screen window
        self.stdscr.clear()
        self.stdscr.border()
        self.stdscr.move(0,2)
        self.stdscr.addstr("Kaboom in ASP (Answer Set Programming)")
        self.stdscr.refresh()

        # draw control window
        cx = self.sx-2 # breadth 
        cy = 5 # height
        segment_offset = int((cx-2) / 4) # for spacing
        self.wcontrol = curses.newwin(cy, cx, 1, 1)
        self.wcontrol.border()
        # -> segment one
        self.wcontrol.addstr(1, 1, "    [n] - new game")
        for c in range(1, segment_offset):
            self.wcontrol.addch(2, c, curses.ACS_HLINE)
        self.wcontrol.addch(2, segment_offset, curses.ACS_URCORNER)
        self.wcontrol.addstr(3, 1, "    [q] - quit")
        self.wcontrol.addch(3, segment_offset, curses.ACS_VLINE)
        # -> segment two
        self.wcontrol.addstr(1, segment_offset+1, "    height: ", curses.A_ITALIC)
        self.wcontrol.addstr(1, segment_offset+15, '[', curses.color_pair(249) | curses.A_REVERSE)
        self.wcontrol.addstr(1, segment_offset+20, ']', curses.color_pair(249) | curses.A_REVERSE)
        self.wcontrol.addch(1, 2*segment_offset, curses.ACS_VLINE)
        self.wcontrol.addstr(2, segment_offset+1, "    width: ", curses.A_ITALIC)
        self.wcontrol.addstr(2, segment_offset+15, '[', curses.color_pair(249) | curses.A_REVERSE)
        self.wcontrol.addstr(2, segment_offset+20, ']', curses.color_pair(249) | curses.A_REVERSE)
        self.wcontrol.addch(2, 2*segment_offset, curses.ACS_VLINE)
        self.wcontrol.addstr(3, segment_offset+1, "    mines: ", curses.A_ITALIC)
        self.wcontrol.addstr(3, segment_offset+15, '[', curses.color_pair(249) | curses.A_REVERSE)
        self.wcontrol.addstr(3, segment_offset+20, ']', curses.color_pair(249) | curses.A_REVERSE)
        self.wcontrol.addch(3, 2*segment_offset, curses.ACS_VLINE)
        # -> segment three
        self.wcontrol.addstr(1, 2*segment_offset+1, "    [wasd / hjkl] - move cursor")
        self.wcontrol.addch(1, 3*segment_offset, curses.ACS_VLINE)
        self.wcontrol.addstr(2, 2*segment_offset+1, "    [m] - mark as mine")
        self.wcontrol.addch(2, 3*segment_offset, curses.ACS_VLINE)
        self.wcontrol.addstr(3, 2*segment_offset+1, "    [x] - reveal tile")
        self.wcontrol.addch(3, 3*segment_offset, curses.ACS_VLINE)
        # -> segment four
        self.wcontrol.addstr(1, 3*segment_offset+1, "    [u] - undo")
        self.wcontrol.addstr(2, 3*segment_offset+1, "    [H] - hint | [D] - debug")
        self.wcontrol.addstr(3, 3*segment_offset+1, "    [r] - recommend")
        self.wcontrol.refresh()

        # draw status bar
        ty = 1 # sTatus bar height
        self.stdscr.addch( (cy+ty+1), 0, curses.ACS_LTEE)
        self.stdscr.addch( (cy+ty+1), self.sx-1, curses.ACS_RTEE)
        self.stdscr.refresh()
        # -> segmant one: shows mine count
        self.wstatus1 = curses.newwin(ty, segment_offset, cy+1, 1)
        self.wstatus1.refresh()
        # -> segmant two: shows hint
        self.wstatus2 = curses.newwin(ty, segment_offset, cy+1, segment_offset+1)
        self.wstatus2.refresh()
        # -> segmant three: shows other messages; a double-segment
        self.wstatus3 = curses.newwin(ty, 2*segment_offset, cy+1, 2*segment_offset+1)
        self.wstatus3.refresh()

        # draw board window
        self.bx = self.sx-2
        self.by = self.sy-cy-ty-2
        self.wboard = curses.newwin(self.by, self.bx, cy+ty+1, 1)
        self.wboard.border(' ', ' ', 0, ' ', curses.ACS_HLINE, curses.ACS_HLINE, ' ', ' ')
        self.wboard.refresh()

        # input windows
        il = 4 # input length
        # -> input one
        self.w_width_input = curses.newwin(1, il, 2, segment_offset+17)
        self.w_width_input.bkgd(' ', curses.color_pair(249) | curses.A_REVERSE | curses.A_BLINK)
        self.w_width_input.refresh()
        # -> input two
        self.w_breadth_input = curses.newwin(1, il, 3, segment_offset+17)
        self.w_breadth_input.bkgd(' ', curses.color_pair(249) | curses.A_REVERSE | curses.A_BLINK)
        self.w_breadth_input.refresh()
        # -> input three
        self.w_mines_input = curses.newwin(1, il, 4, segment_offset+17)
        self.w_mines_input.bkgd(' ', curses.color_pair(249) | curses.A_REVERSE | curses.A_BLINK)
        self.w_mines_input.refresh()

        self.stdscr.move( (3+cy+ty), 4)


    def take_user_input(self):
        curses.echo()
        curses.curs_set(True)

        self.w_width_input.clear()
        self.w_width_input.move(0,0)
        self.w_width_input.refresh()
        bi = self.w_width_input.getstr()

        self.w_breadth_input.clear()
        self.w_breadth_input.move(0,0)
        self.w_breadth_input.refresh()
        bj = self.w_breadth_input.getstr()

        self.w_mines_input.clear()
        self.w_mines_input.move(0,0)
        self.w_mines_input.refresh()
        mi = self.w_mines_input.getstr()

        curses.noecho()

        if int(bi) > (self.by-2) or int(bj) > (int(self.bx/2)):
            self.wstatus3.clear()
            message = "maximal game size is " + str(self.by-2) + " × " + str(int(self.bx/2))
            self.display3(message)
        else:
            return (bi, bj, mi)


    """ displays the current game state and
    also manages different functionality for normal and debug mode
    """
    def display(self, game):
        # remember current cursor
        y,x = self.wboard.getyx()

        # clear screen and draw line
        self.wboard.clear()
        self.wboard.border(' ', ' ', 0, ' ', curses.ACS_HLINE, curses.ACS_HLINE, ' ', ' ')

        if game.debug:
            disp = game.ts(game.get_debug())
            mark_source = game.ans_mines
        else:
            disp = str(game)
            mark_source = game.marks

        # take string and split it by line breaks
        lines = disp.split('\n')
        for i,l in enumerate(lines):
            # move cursor to the correct position
            self.wboard.move(i+1,1)
            # and print
            self.wboard.addstr(l)

        # put markings
        for (i,j) in mark_source:
            (mx,my) = self.ind_to_pos(i,j)
            self.wboard.addch(mx, my, 'm', curses.color_pair(198))

        self.wboard.move(y,x)
        self.wboard.refresh()


    """ displays a message in the second segment of the status bar
    """
    def display2(self, string):
        self.wstatus2.clear()
        self.wstatus2.addstr(string)
        self.wstatus2.refresh()


    """ displays a message in the third segment of the status bar
    """
    def display3(self, string):
        self.wstatus3.clear()
        self.wstatus3.addstr(string)
        self.wstatus3.refresh()

   
    """ outputs the cursor positions to a given move and, if the highlight
    argument is true, changes the background color of the respective tile
    """
    def highlight(self, move, highl):
        y,x = self.ind_to_pos(move[0],move[1])
        if highl:
            self.wboard.addch(y, x, '*', curses.color_pair(47))
        return (y,x)


    """ translates an index of the game board to a position in the window
    """
    def ind_to_pos(self, i, j):
        return int(i+1), int(2*j+1)


    """ translates a window position to a game board index
    input must be odd
    """
    def pos_to_ind(self, y, x):
        if x%2 == 0:
            raise ValueError("wboard xpos must be odd but is " + str(x))

        return int(y-1), int(x/2)
