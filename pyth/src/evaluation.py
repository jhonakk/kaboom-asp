# this is a script for generating data to analyze
# difficulty of a game based on mine density

import copy
import json
import random
import subprocess
import sys

import game as g


# a special variant of callasp to return clingo --stats
def solve_benchmark(list_of_files):
    pre = '../../asp/'
    files = list_of_files
    code = ""

    for fi in files:
        with open(pre+fi) as f:
            content = f.read().splitlines()

        for line in content:
            code = code + line + '\n'

    # call clingo
    clingo_path = '/bin/clingo'
    clingo_options = ['--stats=2', '--outf=2','-n 0']
    clingo_command = [clingo_path] + clingo_options

    input = code.encode()
    process = subprocess.Popen(clingo_command, stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    output, error = process.communicate(input)
    result = json.loads(output.decode())
    if result['Result'] == 'SATISFIABLE':
        models = result['Models']['Number']
        time = result['Time']['Total']
        rules = result['Stats']['LP']['Rules']['Final']
        constraints = result['Stats']['Problem']['Constraints']['Sum']
    else:
        raise ValueError("Result is " + str(result['Result']))

    return (models, time,rules,constraints)



# print head of table
se = ", " # separator
head = "no{s}size{s}minedens{s}revealed{s}models{s}time{s}rules{s}constraints\n".format(s=se)

with open("evalall", 'w', encoding="utf-8") as f:
    f.write(head)


# declare a list of instances to test on
instances = []
for i in range(18):
    tmp = "eval" + str(i) + ".lp"
    instances.append(tmp)

game = g.Game(2,2,2)

# for every instance
for en, inst in enumerate(instances):

    # initialise
    game.asp_to_py(inst)
    game.py_to_asp(game.board)

    # calculate revealed
    revealed = len(game.get_revealed())

    # call solver_benchmark
    lof = ["kaboom.lp", "validator.lp", "instances/inst-tmp.lp",\
           "edges.lp", "candidates.lp", "recommender.lp"]
    (models, time,rules,constraints) = solve_benchmark(lof)

    # write stats to file
    size = game.bi * game.bj
    dens = game.num_mines / size
    line =  str(en) + ", " +\
            str(size) + ", " +\
            str(dens) + ", " +\
            str(revealed) + ", " +\
            str(models) + ", " +\
            str(time) + ", " +\
            str(rules) + ", " +\
            str(constraints) + "\n"
    print(line)
    with open("evalall", 'a', encoding="utf-8") as f:
        f.write(line)
