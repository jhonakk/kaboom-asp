# this is a script for generating data in order to analyze
# difficulty of a game based on mine density

import callasp
import copy
import random

import game as g


# print head of table
se = ", " # separator
head = "density{s}moves\n".format(s=se)

with open("anadatarandom", 'w', encoding="utf-8") as f:
    f.write(head)

game = g.Game(2,2,2)
for a in range(0, 99):
    for b in range(10):
        # generate a game
        del game
        game = g.Game(10,10,a+1)
        movecount = 0
        while game.state == g.GameState.PENDING:
            game.py_to_asp(game.board)
            game.validate()

            ## this is for random moves
            moves = game.valid_choices()
            rec = random.sample(moves, 1)
            game.update(rec[0])

            ## and this for recommended moves
            #rec = game.recommend()
            #game.update(rec)

            movecount += 1
            game.check_win()

        if game.state == g.GameState.LOSE:
            print(rec)
            print(game.board_to_string(game.get_debug()))
            raise ValueError("lost")

        # write data to file
        line = str((a+1)/100) + ", " + str(movecount) + "\n"
        print(line)
        with open("anadatarandom", 'a', encoding="utf-8") as f:
            f.write(line)
